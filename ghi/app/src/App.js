import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatCreateForm from './HatCreateForm';
import HatsList from './HatsList';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesCreateForm from './ShoesCreateForm';
import ShoesList from './ShoesList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path = "" element= {<HatsList hats = {props.hats} />} /> 
            <Route path = "new" element= {<HatCreateForm/>} /> 
          </Route>
          <Route path="shoes">
            <Route path = "shoes" element= {<ShoesList shoes = {props.shoes} />} />
            <Route path = "new" element={<ShoesCreateForm/>} />
          </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;