import React from 'react';

class HatCreateForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      style: '',
      color: '',
      fabric: '',
      picture_url: '',
      location_id: '',
      locations: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleStyleChange = this.handleStyleChange.bind(this);
    this.handleFabricChange = this.handleFabricChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
  }

  handleStyleChange(event) {
    const value = event.target.value;
    this.setState({ style: value });
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleFabricChange(event) {
    const value = event.target.value;
    this.setState({ fabric: value });
  }

  handlePictureUrlChange(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({ location_id: value }); 
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    console.log('submit: ', data)
    delete data.locations

    const locationUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      this.setState({
        style: '',
        color: '',
        fabric: '',
        picture_url: '',
        location_id: '',
      });
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({locations: data.locations});
        // const selectTag = document.querySelector('#closet_location')
        // for (let loc of data.locations){
        //     const option = document.createElement('option')
        //     option.value = loc.closet_name
        //     option.innerHTML = loc.closet_name
        //     selectTag.appendChild(option)
        // }
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureUrlChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="" id="closet_location" >Choose storage location</option>
                  {this.state.locations.map(loc=> {
                    return (
                      <option key={loc.id} value={loc.id}>{loc.closet_name + "-" + loc.section_number + "/" + loc.shelf_number}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Store</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatCreateForm;