import React from 'react'

function ShoesList(props) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th className="table-primary"></th>
              <th>Manufacturer</th>
              <th>Shoe Name</th>
              <th>Shoe Color</th>
              <th>Shoe Photo</th>
            </tr>
          </thead>
          <tbody>
            {console.log(props.arr)}
            {props.shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.shoe_name }</td>
                  <td>{ shoe.shoe_color }</td>
                  <td>{ shoe.shoe_photo_url }</td>
                  <td className="delete">
                    <button onClick={() => this.delete(shoe.id)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
}

export default ShoesList;