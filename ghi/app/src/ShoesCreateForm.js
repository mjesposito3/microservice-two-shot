import React from 'react';

class ShoesCreateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: '',
      shoe_name: '',
      shoe_color: '',
      shoe_photo_url: '',
      bin_id: '',
      bins: [],
    }

    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleShoeNameChange = this.handleShoeNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePhotoChange = this.handlePhotoChange.bind(this);
    this.handleBinChange = this.handleBinChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleShoeNameChange(event) {
    const value = event.target.value;
    this.setState({ shoe_name: value });
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ email: value });
  }

  handlePhotoChange(event) {
    const value = event.target.value;
    this.setState({ email: value });
  }

  handleBinChange(event) {
    const value = event.target.value;
    this.setState({ email: value });
  }


  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.bins;
    console.log(data)

    const ShoesUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(ShoesUrl, fetchConfig);

    if (response.ok) {
      this.setState({
        manufacturer: '',
        shoe_name: '',
        shoe_color: '',
        shoe_photo_url: '',
        bins: []
      });
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ bin: data.bins });
    }
  }
  

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleManufacturerChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                <label htmlFor="style">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleShoeNameChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Shoe Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePhotoChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleBinChange} required name="bin" id="bin" className="form-select">
                  <option value="" id="closet_location" >Choose Storage Bin</option>
                  {this.state.bins.map(loc=> {
                    return (
                      <option key={loc.id} value={loc.id}>{loc.closet_name + "-" + loc.section_number + "/" + loc.shelf_number}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Store</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoesCreateForm;
