function HatsList(props) {
    if (props.hats === undefined){
      return null
    }
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th className="col-3"></th>
            <th>Style</th>
            <th>Color</th>
            <th>Fabric</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td><img src={ hat.picture_url} className="img-thumbnail" /></td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td>{ hat.fabric }</td>
                <td>{hat.location_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
  export default HatsList;