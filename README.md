# Wardrobify

Team:

* Caleb - Shoes
* Mark - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I will create a Shoe model with a BinVO (Bin Value Object) and copy data form the wardrobe app over to the shoes api via polling

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I will be creating a Hat Model with a LocationVO (Value Object) and copy the data from the wardrobe app over the the hats api via polling.