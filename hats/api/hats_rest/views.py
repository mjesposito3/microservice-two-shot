from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "closet_name",
        "section_number",
        "shelf_number",
    ]



class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        'location_id', 
        'fabric', 
        'color', 
        'style', 
        'picture_url',
        'id',
        "location"
        ]
    
    encoders= {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "POST":
        content = json.loads(request.body)
        print(content)
        hat = Hat.objects.create(**content)
        return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )

    else: #GET
        hats = Hat.objects.all()
        print(hats)
        return JsonResponse(
                hats,
                encoder=HatEncoder,
                safe=False
            )
    
@require_http_methods(["GET", "PUT", "DELETE"])
def api_hats_detail(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "** hand wave ** These aren't the droids you're looking for."})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)

            props = ['location_id', 'fabric', 'color', 'style', 'picture_url']
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
                )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "** hand wave ** These aren't the droids you're looking for."})
            response.status_code = 404
            return response
    else: # DELETE
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "BOI! You deleted dat sheeeee"})