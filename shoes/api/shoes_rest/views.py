from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
    "closet_name", 
    "bin_number", 
    "bin_size", 
    "import_href", 
    "id"
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "shoe_name",
        "shoe_color",
        "shoe_photo_url",
        "shoe_bin_location",
    ]

    encoders = {"bin": BinVOEncoder}

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin = content["bins"]
            bin = BinVO.objects.get(import_href=bin_vo_id)
            content["bins"] = bin
            shoes = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoesListEncoder,
                safe=False,
            )
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

@require_http_methods(["GET"])
def api_detail_shoe(request,pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})