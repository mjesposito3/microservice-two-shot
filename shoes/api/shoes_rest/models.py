from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=150)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)
    import_href = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=126)
    shoe_name = models.CharField(max_length=126)
    shoe_color = models.CharField(max_length=56)
    shoe_photo_url = models.URLField(blank=True,null=True)

    shoe_bin_location = models.ForeignKey(
        BinVO, 
        related_name="shoe_bin_location", 
        on_delete=models.CASCADE
        )
